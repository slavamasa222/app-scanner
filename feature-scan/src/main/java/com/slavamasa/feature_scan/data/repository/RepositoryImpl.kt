package com.slavamasa.feature_scan.data.repository

import com.slavamasa.feature_scan.data.data_source.AppsStorage
import com.slavamasa.feature_scan.domain.models.AppInfo
import com.slavamasa.feature_scan.domain.repository.Repository

class RepositoryImpl(private val appStorage: AppsStorage) : Repository {
    override fun getInstalledApps(): List<AppInfo> = appStorage.getAllAppsOnDevice()
}