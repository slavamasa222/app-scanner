package com.slavamasa.feature_scan.data.data_source

import android.content.Context
import android.content.pm.PackageManager
import com.slavamasa.feature_scan.domain.models.AppInfo
import java.util.*


class AppsStorage(private val context: Context) {

    fun getAllAppsOnDevice(): List<AppInfo> {

        val apps = context.packageManager.getInstalledApplications(PackageManager.GET_META_DATA)
        val appInfoList: MutableList<AppInfo> = mutableListOf()
        for (i in 0 until apps.size) {
            appInfoList.add(
                AppInfo(i, apps[i])
            )
        }
        return appInfoList
    }

}