package com.slavamasa.feature_scan.domain.usecases

import com.slavamasa.feature_scan.domain.repository.Repository

class GetInstalledApps(private val repository: Repository) {
    fun getInstalledApps() = repository.getInstalledApps()
}