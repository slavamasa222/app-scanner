package com.slavamasa.feature_scan.domain.models

import android.content.pm.ApplicationInfo

data class AppInfo(
    val id: Int,
    val appInfo: ApplicationInfo
)
