package com.slavamasa.feature_scan.domain.repository

import com.slavamasa.feature_scan.domain.models.AppInfo

interface Repository {
    fun getInstalledApps(): List<AppInfo>
}