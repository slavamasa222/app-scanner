package com.slavamasa.feature_scan.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.slavamasa.feature_scan.R
import com.slavamasa.feature_scan.databinding.ItemRvScanResultsBinding
import com.slavamasa.feature_scan.domain.models.AppInfo
import com.slavamasa.feature_scan.presentation.screens.ScanResultFragmentDirections

class ScanResultsAdapter : RecyclerView.Adapter<ScanResultsAdapter.ViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<AppInfo>() {
        override fun areItemsTheSame(oldItem: AppInfo, newItem: AppInfo): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: AppInfo,
            newItem: AppInfo
        ): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(private val binding: ItemRvScanResultsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener {
                val action = ScanResultFragmentDirections
                    .actionScanResultFragmentToSingleScanItemFragment(
                        differ.currentList[bindingAdapterPosition].id
                    )
                it.findNavController().navigate(action)
            }
        }

        fun bind() {
            val packageManager = itemView.context.packageManager
            val appInfo = differ.currentList[bindingAdapterPosition]

            binding.appName.text = packageManager.getApplicationLabel(appInfo.appInfo)
            Glide.with(itemView.context)
                .load(appInfo.appInfo.loadIcon(packageManager))
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(binding.appIcon)



        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemRvScanResultsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = differ.currentList.size
}