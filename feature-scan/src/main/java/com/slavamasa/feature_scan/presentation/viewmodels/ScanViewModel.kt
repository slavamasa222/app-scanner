package com.slavamasa.feature_scan.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.slavamasa.feature_scan.domain.usecases.GetInstalledApps

class ScanViewModel(getInstalledApps: GetInstalledApps) : ViewModel() {

    val apps = liveData { emit(getInstalledApps.getInstalledApps()) }

}