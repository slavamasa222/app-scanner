package com.slavamasa.feature_scan.presentation.screens

import android.content.pm.ApplicationInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.slavamasa.feature_scan.R
import com.slavamasa.feature_scan.databinding.FragmentSingleScanItemBinding
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.slavamasa.feature_scan.presentation.viewmodels.ScanViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class SingleScanItemFragment : Fragment() {

    private lateinit var binding: FragmentSingleScanItemBinding
    private val navArgs: SingleScanItemFragmentArgs by navArgs()
    private val viewModel by sharedViewModel<ScanViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_single_scan_item, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners()

        viewModel.apps.observe(viewLifecycleOwner, { appInfoList ->
            val packageManager = requireContext().packageManager
            appInfoList.find { it.id == navArgs.id }?.let { appInfo ->
                binding.singleAppInfo.apply {
                    Glide.with(requireContext())
                        .load(appInfo.appInfo.loadIcon(packageManager))
                        .placeholder(R.drawable.ic_baseline_image_24)
                        .into(appIcon)

                    appName.text = packageManager.getApplicationLabel(appInfo.appInfo)
                    appSize.text = getFormattedSize(File(appInfo.appInfo.publicSourceDir).length())
                    appTargetSdkVersion.text = appInfo.appInfo.targetSdkVersion.toString()
                    appInstallationDate.text = getFormattedDate(appInfo.appInfo)
                }

            }

        })

    }

    private fun initClickListeners() {
        binding.themeSwitcher.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(
                when (AppCompatDelegate.getDefaultNightMode()) {
                    AppCompatDelegate.MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_YES
                    else -> AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }

        binding.more.setOnClickListener {
            Toast.makeText(requireContext(), "DOES NOTHING", Toast.LENGTH_SHORT).show()
        }

        binding.backBtn.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun getFormattedDate(appInfo: ApplicationInfo): String {
        val sdf = SimpleDateFormat("dd.MM.yyyy", Locale("ru", "RU"))
        return sdf.format(File(appInfo.publicSourceDir).lastModified())
    }

    private fun getFormattedSize(size: Long): String {
        var size: Float = size.toFloat()
        var prefixCounter = 0
        while (size > 1024 && prefixCounter < 3) {
            size /= 1024
            prefixCounter++
        }
        size = String.format(Locale.US, "%.2f", size).toFloat()


        return when (prefixCounter) {
            0 -> "$size B"
            1 -> "$size KB"
            2 -> "$size MB"
            else -> "$size GB"
        }
    }

}