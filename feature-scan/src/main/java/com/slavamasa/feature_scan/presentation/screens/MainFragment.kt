package com.slavamasa.feature_scan.presentation.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.slavamasa.feature_scan.R
import com.slavamasa.feature_scan.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners()


    }

    private fun initClickListeners() {
        binding.themeSwitcher.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(
                when (AppCompatDelegate.getDefaultNightMode()) {
                    AppCompatDelegate.MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_YES
                    else ->AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }

        binding.more.setOnClickListener {
            Toast.makeText(requireContext(), "DOES NOTHING", Toast.LENGTH_SHORT).show()
        }

        binding.buttonScan.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_scanResultFragment)
        }
    }

}