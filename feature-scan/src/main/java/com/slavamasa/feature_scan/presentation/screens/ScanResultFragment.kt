package com.slavamasa.feature_scan.presentation.screens

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.slavamasa.feature_scan.R
import com.slavamasa.feature_scan.databinding.FragmentScanResultBinding
import com.slavamasa.feature_scan.presentation.adapters.ScanResultsAdapter
import com.slavamasa.feature_scan.presentation.viewmodels.ScanViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ScanResultFragment : Fragment() {


    private lateinit var binding: FragmentScanResultBinding
    private lateinit var scanResultsAdapter: ScanResultsAdapter
    private val viewModel by sharedViewModel<ScanViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan_result, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners()
        initRecyclerView()

        viewModel.apps.observe(viewLifecycleOwner, {
            scanResultsAdapter.differ.submitList(it)
        })
    }

    private fun initClickListeners() {
        binding.themeSwitcher.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(
                when (AppCompatDelegate.getDefaultNightMode()) {
                    AppCompatDelegate.MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_YES
                    else -> AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }

        binding.more.setOnClickListener {
            Toast.makeText(requireContext(), "DOES NOTHING", Toast.LENGTH_SHORT).show()
        }
    }



    private fun initRecyclerView() {
        scanResultsAdapter = ScanResultsAdapter()
        scanResultsAdapter.stateRestorationPolicy =
            RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        scanResultsAdapter.differ.submitList(listOf())
        binding.recyclerViewScanResults.adapter = scanResultsAdapter
        binding.recyclerViewScanResults.layoutManager = GridLayoutManager(requireContext(), 2)
    }

}