package com.slavamasa.appscanner.di

import com.slavamasa.feature_scan.data.data_source.AppsStorage
import com.slavamasa.feature_scan.data.repository.RepositoryImpl
import com.slavamasa.feature_scan.domain.repository.Repository
import com.slavamasa.feature_scan.domain.usecases.GetInstalledApps
import com.slavamasa.feature_scan.presentation.viewmodels.ScanViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    single<AppsStorage> { AppsStorage(get()) }
    single<Repository> { RepositoryImpl(appStorage = get()) }
}

val domainModule = module {
    factory<GetInstalledApps> { GetInstalledApps(repository = get()) }
}

val presentationModule = module {
    viewModel<ScanViewModel> { ScanViewModel(getInstalledApps = get()) }
}