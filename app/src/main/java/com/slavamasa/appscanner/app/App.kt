package com.slavamasa.appscanner.app

import android.app.Application
import com.slavamasa.appscanner.di.dataModule
import com.slavamasa.appscanner.di.domainModule
import com.slavamasa.appscanner.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(dataModule, domainModule, presentationModule)
        }
    }
}